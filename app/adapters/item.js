import JSONAPIAdapter from '@ember-data/adapter/json-api';
import ENV from 'wishlist/config/environment';

export default class ItemAdapter extends JSONAPIAdapter {
  host = ENV.APP.WISHLIST_API_HOST;
  namespace = 'api/wishlist';
  apiToken = '';

  get headers() {
    return {
      x_api_token: this.apiToken,
    };
  }

  createRecord() {
    throw new Error('Unsupported feature');
  }

  updateRecord() {
    throw new Error('Unsupported feature');
  }

  deleteRecord() {
    throw new Error('Unsupported feature');
  }

  urlForQuery(query) {
    const { wishlistId } = query;

    return `${this.host}/${this.namespace}/${wishlistId}/items`;
  }

  query(store, type, query) {
    const { apiToken } = query;

    this.apiToken = apiToken;

    return super.query(...arguments);
  }
}
