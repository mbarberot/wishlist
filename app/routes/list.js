import Route from '@ember/routing/route';
import { service } from '@ember/service';

export default class ListRoute extends Route {
  @service store;

  async model({ token }) {
    const decoded = atob(token);
    const { apiToken, wishlistId } = JSON.parse(decoded);

    const wishlist = await this.store.queryRecord('wishlist', {
      apiToken,
      wishlistId,
    });
    const items = await this.store.query('item', { apiToken, wishlistId });

    return {
      title: wishlist.title,
      bannerUrl: wishlist.bannerUrl,
      items,
    };
  }
}
