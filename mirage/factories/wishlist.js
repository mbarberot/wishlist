import { Factory } from 'miragejs';

function nextInt(max) {
  return Math.floor(Math.random() * (max - 1));
}

export default Factory.extend({
  title() {
    const titles = [
      'Interdum mauris pellentesque hendrerit, dolor ac',
      'Faucibus a pellentesque sit amet',
      'Imperdiet sed euismod nisi porta',
      'Lorem ipsum viverra aptent, tempor',
      'Ad malesuada vitae quam, nam',
      'Fames laoreet nam dapibus, lectus',
      'Quisque luctus sollicitudin, taciti',
      'Hac fringilla per, vitae',
      'Semper eros libero',
      'Sem venenatis potenti',
      'Porttitor erat semper',
      'Viverra ipsum',
      'Ut cursus',
      'Sagittis tristique',
      'Cubilia',
      'Ultricies',
      'Sem',
      'Massa',
    ];
    return titles[nextInt(titles.length)];
  },

  bannerUrl() {
    return `https://picsum.photos/id/${nextInt(300)}/1080/200`;
  },

  afterCreate(wishlist, server) {
    wishlist.update({
      items: server.createList('item', 10),
    });
  },
});
