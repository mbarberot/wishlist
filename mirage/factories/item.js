import { Factory } from 'miragejs';

function nextInt(max) {
  return Math.floor(Math.random() * (max - 1));
}

export default Factory.extend({
  title() {
    const titles = [
      'Interdum mauris pellentesque hendrerit, dolor ac',
      'Faucibus a pellentesque sit amet',
      'Imperdiet sed euismod nisi porta',
      'Lorem ipsum viverra aptent, tempor',
      'Ad malesuada vitae quam, nam',
      'Fames laoreet nam dapibus, lectus',
      'Quisque luctus sollicitudin, taciti',
      'Hac fringilla per, vitae',
      'Semper eros libero',
      'Sem venenatis potenti',
      'Porttitor erat semper',
      'Viverra ipsum',
      'Ut cursus',
      'Sagittis tristique',
      'Cubilia',
      'Ultricies',
      'Sem',
      'Massa',
    ];
    return titles[nextInt(titles.length)];
  },

  description() {
    if (nextInt(100) <= 75) {
      return null;
    }

    const descriptions = [
      'Vert',
      'Noir',
      'Bleu',
      'Image non contractuelle',
      'Lien non contractuel',
      'Taille moyenne',
      'En vinyle',
      'Format "broché"',
      'Version PC',
    ];
    return descriptions[nextInt(descriptions.length)];
  },

  price() {
    return nextInt(1000);
  },

  image() {
    return `https://picsum.photos/id/${nextInt(300)}/200`;
  },

  link() {
    return 'https://www.google.fr';
  },

  reserved() {
    return nextInt(100) <= 25;
  },
});
