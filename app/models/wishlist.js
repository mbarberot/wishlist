import Model, { attr, hasMany } from '@ember-data/model';

export default class Wishlist extends Model {
  @attr('string') title;
  @attr('string') bannerUrl;
  @hasMany('item', { async: true }) items;
}
