export default function () {
  this.urlPrefix = 'http://localhost:8080';
  this.namespace = '/api';

  this.get('/wishlist/:id/items', (schema) => {
    return schema.items.all();
  });

  this.get('/wishlist/:id');
}
