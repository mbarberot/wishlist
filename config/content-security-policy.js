'use strict';

module.exports = function (environment) {
  let CSP = {
    delivery: ['header'],
    enabled: true,
    policy: {
      'connect-src': ["'self'", 'https://notion-frontend-api.herokuapp.com'],
      'script-src': ["'self'", 'https://fonts.googleapis.com'],
    },
  };

  if (environment === 'development') {
    CSP.enabled = false;
  }

  return CSP;
};
