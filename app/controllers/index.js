import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class IndexController extends Controller {
  @tracked wishlistId = '';
  @tracked apiToken = '';
  @tracked link = '';

  @action
  generateLink(e) {
    e.preventDefault();

    if (this.apiToken && this.wishlistId) {
      const encryptedToken = generateToken(this.apiToken, this.wishlistId);

      this.link = generateLink(encryptedToken);
    }

    return '';
  }
}

function generateToken(apiToken, wishlistId) {
  return btoa(JSON.stringify({ apiToken, wishlistId }));
}

function generateLink(token) {
  return `${window.location}#/list/${token}`;
}
