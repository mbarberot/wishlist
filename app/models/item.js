import Model, { attr } from '@ember-data/model';

export default class Item extends Model {
  @attr('string') title;
  @attr('string') description;
  @attr('number') price;
  @attr('string') image;
  @attr('string') link;
  @attr('boolean') reserved;
}
